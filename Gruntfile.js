module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig( {
        pkg: grunt.file.readJSON( 'package.json' ),
        sass: {
            dist: {
                options: {
                    style: 'nested'
                },
                files: {
                    'assets/css/app.css': 'assets/scss/app.scss'
                }
            }
        },

        watch: {
            sass: {
                files: 'assets/scss/**/*.scss',
                tasks: ['sass','autoprefixer','cssmin']
            },
            js: {
                files: ['assets/js/app/*.js','assets/js/inc/*.js','assets/js/vendor/*.js'],
                tasks: ['requirejs']
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ie 8', 'ie 9']
            },
            pub: {
                src: 'assets/css/app.css'
            }
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                sourceMap: true
            },
            dist: {
                files: {
                    'assets/css/app.min.css': ['assets/css/app.css']
                }
            }
        },

        requirejs: {
            compile: {
                options: {
                    mainConfigFile: "assets/js/config.js",
                }
            }
        },

        bump: {
            options: {
                files: [ 'package.json', 'style.css' ],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: [ '-a' ],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: true,
                pushTo: 'origin',
            }
        },

    } );

    grunt.loadNpmTasks( 'grunt-contrib-watch' );
    grunt.loadNpmTasks( 'grunt-sass' );
    grunt.loadNpmTasks( 'grunt-autoprefixer' );
    grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-requirejs' );
    grunt.loadNpmTasks( 'grunt-bump' );

    grunt.registerTask( 'default', ['sass','autoprefixer','cssmin', 'requirejs'] );
};
