# Atomic + Grunt #

A custom WP theme that uses the Atomicdocs styleguide generator and RequireJS for JS module management. Uses NodeJS and Grunt for compiling.

### What is this repository for? ###

Honestly, just as a fun side thing. But, the base styles are made to go into any WP theme. It also has some handy mixins and variables file. Possibly use portions of this as a boilerplate.

### How do I get set up? ###

1. Get the git repo via a clone
2. Run `npm install`
3. That's it! No Bower here.

### Using ###

#### Grunt ####
To compile, simply run the `grunt` command from the CLI. You can also use `grunt watch` if you're feeling lazy.

#### Atomicdocs ####
To look at the styleguide, go to where your domain points to the project and use "/atomic-core/". So something like: `http://atomic-grunt.dev/atomic-core/` in the browser. 

If you need more modules/components/whatever, you should be managing them through the AtomicDocs interface. If you have things you don't want to show up there, you can make them manually. Things that fall into this category can probably be grouped into `vendor` and `util` already though.