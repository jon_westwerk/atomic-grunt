var APP_MODULES = [
	// Libraries / Third Party
	'jquery',
	// a11y
	'vendor/frend/fraccordion.min',
	'vendor/frend/frbypasslinks.min',
	'vendor/frend/frdialogmodal.min',
	'vendor/frend/froffcanvas.min',
	'vendor/frend/frtabs.min',
	'vendor/frend/frtooltip.min',
	// Custom Modules
	'inc/_module1',
	'inc/_module2',
];

require( APP_MODULES, function ( jQuery, Fraccordion, Frbypasslinks, Frdialogmodal, Froffcanvas, Frtabs, Frtooltip ){

	// a11y
	// You can edit the options for these plugins here.
	// https://frend.co/

	var a11y_accordions  = Fraccordion();
	var a11y_bypasslinks = Frbypasslinks();
	var a11y_dialogmodal = Frdialogmodal();
	var a11y_offcanvas   = Froffcanvas();
	var a11y_tabs        = Frtabs();
	var a11y_tooltips    = Frtooltip( {
		// String - Container selector, hook for JS init() method
		selector: '.fr-tooltip',
		// String - Selector to define the tooltip element
		tooltipSelector: '.fr-tooltip-tooltip',
		// String - Selector to define the toggle element controlling the tooltip
		toggleSelector: '.fr-tooltip-toggle',
		// String - Prefix for the id applied to each tooltip as a reference for the toggle
		tooltipIdPrefix: 'tooltip',
		// String - Class name that will be added to the selector when the component has been initialised
		readyClass: 'fr-tooltip--is-ready',
	} );

	// Our Modules
	var module1 = require( 'inc/_module1' );
	var module2 = require( 'inc/_module2' );

	// Init code
	jQuery( 'html' ).removeClass( 'no-js' ).addClass( 'js' );

	// Do the things with our modules
	module1.init();
	module2.init();

} );

// Define jQuery as a module since it is being loaded outside of Requirejs and "empty:" doesn't work.
define( 'jquery', [], function () {
    return jQuery;
} );
