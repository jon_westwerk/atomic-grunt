/**
 * This config is used during the build process.
 */
require.config( {
    baseUrl: "./",
    paths: {
		jquery: "empty:",
        gsap: "vendor/TweenMax.min",
        svgxuse: "vendor/svgxuse",
    },
    name: "app/app",
    out: "main.js",
    preserveLicenseComments: false,
    generateSourceMaps: true,
} );

