<!--components/base/Button.php-->

<a href="#" class="button">Default</a>

<a href="#" class="button button--secondary">Secondary</a>

<a href="#" class="button button--tertiary">Tertiary</a>

<a href="#" disabled class="button">Disabled</a>

<a href="#" class="button button--text">Text Button</a>

<button type="button" class="button button--text">Text Button</button>