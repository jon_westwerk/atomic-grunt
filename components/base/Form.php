<!--components/base/Form.php-->

<form action="#" autocomplete="off">
	<ul>
		<li class="gfield">
			<label for="f1">Text Field</label>
			<div class="ginput_container">
				<input id="f1" type="text">
			</div>
		</li>
		<li class="gfield">
			<label for="f2">Password Field</label>
			<div class="ginput_container">
				<input id="f2" type="password">
			</div>
		</li>
		<li class="gfield">
			<label for="f3">URL</label>
			<div class="ginput_container">
				<input id="f3" type="url" placeholder="http://www.example.com">
			</div>
		</li>
		<li class="gfield">
			<label for="f4">Email</label>
			<div class="ginput_container">
				<input id="f4" type="email" placeholder="john.doe@example.com">
			</div>
		</li>
		<li class="gfield">
			<label for="f5">Search</label>
			<div class="ginput_container">
				<input id="f5" type="search">
			</div>
		</li>
		<li class="gfield">
			<label for="f6">Number</label>
			<div class="ginput_container">
				<input id="f6" type="number">
			</div>
		</li>
		<li id="field_2_4" class="gfield field_sublabel_below field_description_below">
			<label class="gfield_label" for="input_2_4">Textarea</label>
			<div class="ginput_container">
				<textarea name="input_4" id="input_2_4" class="textarea medium" tabindex="2" aria-invalid="false" rows="10" cols="50">
				</textarea>
			</div>
		</li>
		<li id="field_2_5" class="gfield field_sublabel_below field_description_below">
			<label class="gfield_label" for="input_2_5">Select</label>
			<div class="ginput_container ginput_container_select">
				<select name="input_5" id="input_2_5" class="medium gfield_select" tabindex="3" aria-invalid="false">
					<option value="First Choice">First Choice</option>
					<option value="Second Choice">Second Choice</option>
					<option value="Third Choice">Third Choice</option>
				</select>
			</div>
		</li>
		<li id="field_2_6" class="gfield field_sublabel_below field_description_below">
			<label class="gfield_label" for="input_2_6">Multiselect</label>
			<div class="ginput_container ginput_container_multiselect">
				<select multiple="multiple" size="7" name="input_6[]" id="input_2_6" class="medium gfield_select" tabindex="4">
					<option value="First Choice">First Choice</option>
					<option value="Second Choice">Second Choice</option>
					<option value="Third Choice">Third Choice</option>
				</select>
			</div>
		</li>
		<li class="gfield">
			<label class="gfield_label">Checkboxes</label>
			<div class="ginput_container ginput_container_checkbox">
				<ul class="gfield_checkbox">
					<li>
						<input name="input_1.1" value="First Choice" id="choice_2_1_1" tabindex="6" type="checkbox">
						<label for="choice_2_1_1" id="label_2_1_1">First Choice</label>
					</li>
					<li class="gchoice_2_1_2">
						<input name="input_1.2" value="Second Choice" id="choice_2_1_2" tabindex="7" type="checkbox">
						<label for="choice_2_1_2" id="label_2_1_2">Second Choice</label>
					</li>
					<li class="gchoice_2_1_3">
						<input name="input_1.3" value="Third Choice" id="choice_2_1_3" tabindex="8" type="checkbox">
						<label for="choice_2_1_3" id="label_2_1_3">Third Choice</label>
					</li>
				</ul>
			</div>
		</li>
		<li class="gfield field_sublabel_below field_description_below">
			<label class="gfield_label">Radios</label>
			<div class="ginput_container ginput_container_radio">
				<ul class="gfield_radio">
					<li class="gchoice_2_2_0">
						<input name="input_2" value="First Choice" id="choice_2_2_0" tabindex="9" type="radio">
						<label for="choice_2_2_0" id="label_2_2_0">First Choice</label>
					</li>
					<li class="gchoice_2_2_1">
						<input name="input_2" value="Second Choice" id="choice_2_2_1" tabindex="10" type="radio">
						<label for="choice_2_2_1" id="label_2_2_1">Second Choice</label>
					</li>
					<li class="gchoice_2_2_2">
						<input name="input_2" value="Third Choice" id="choice_2_2_2" tabindex="11" type="radio">
						<label for="choice_2_2_2" id="label_2_2_2">Third Choice</label>
					</li>
				</ul>
			</div>
		</li>
	</ul>
</form>