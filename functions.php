<?php
/**
 * Theme functions
 *
 * @package wormwood
 */

require_once( 'inc/setup.php' );
require_once( 'inc/init.php' );
require_once( 'inc/enqueue.php' );
require_once( 'inc/custom.php' );
require_once( 'inc/cpt.php' );
require_once( 'inc/template-tags.php' );
require_once( 'inc/rewrites.php' );
