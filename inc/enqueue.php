<?php
/**
 * Theme enqueues
 *
 * @package wormwood
 */

/**
 * Enqueue scripts.
 */
if ( ! function_exists( 'wormwood_enqueue' ) ) {
	/**
	 * Enqueue theme styles and scripts
	 */
	function wormwood_enqueue() {

		// Primary stylesheet.
		wp_enqueue_style( 'primary-style', get_template_directory_uri() . '/assets/css/app-min.css' );

		// Primary JS.
		if ( SCRIPT_DEBUG || WP_DEBUG ) {
			wp_enqueue_script( 'development-js', get_template_directory_uri() . '/assets/js/development.js', array( 'jquery' ), '1.0.0', true );
		} else {
			wp_enqueue_script( 'production-js', get_template_directory_uri() . '/assets/js/production-min.js', array( 'jquery' ), '1.0.0', true );
		}

	}

	add_action( 'wp_enqueue_scripts', 'wormwood_enqueue' );
}

if ( ! function_exists( 'wormwood_handle_localize' ) ) {
	/**
	 * Handle localizing data to the correct version of the script.
	 *
	 * @param string $object_name - The name of the localized object.
	 * @param mixed  $object_value - The value to place in the localized object.
	 */
	function wormwood_handle_localize( $object_name, $object_value ) {
		if ( SCRIPT_DEBUG || WP_DEBUG ) {
			wp_localize_script( 'development-js', $object_name, $object_value );
		} else {
			wp_localize_script( 'production-js', $object_name, $object_value );
		}
	}
}
