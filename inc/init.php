<?php
/**
 * Init functions
 *
 * @package wormwood
 */

if ( ! function_exists( 'wormwood_init' ) ) {
	/**
	 * Theme init functionality
	 */
	function wormwood_init() {

	}

	add_action( 'init', 'wormwood_init' );
}
