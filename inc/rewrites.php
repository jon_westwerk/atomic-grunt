<?php
/**
 * Theme Redirects & Rewrites
 *
 * @package wormwood
 */

if ( ! function_exists( 'wormwood_change_posts_link' ) ) {
	/**
	 * Change Posts Permalink
	 *
	 * @param string  $link - the current link.
	 * @param WP_Post $post - the Post object.
	 *
	 * @return string|void
	 */
	function wormwood_change_posts_link( $link, $post ) {
		if ( 'post' !== get_post_type( $post ) ) {
			return $link;
		}

		$posts_name = wormwood_get_blog_page_name();
		$slug       = $post->post_name;
		$link = site_url( sprintf( '/%s/%s', $posts_name, $slug ) );

		return $link;
	}
}
add_filter( 'post_link', 'wormwood_change_posts_link', 100, 2 );

if ( ! function_exists( 'wormwood_posts_rewrite' ) ) {
	/**
	 * Add Product rewrite
	 */
	function wormwood_posts_rewrite() {
		$posts_name = wormwood_get_blog_page_name();
		add_rewrite_rule( '^' . $posts_name . '/page/([0-9]{1,})/?', 'index.php?pagename=' . $posts_name . '&paged=$matches[1]', 'top' );
		add_rewrite_rule( '^' . $posts_name . '/([^/]+)/?', 'index.php?name=$matches[1]', 'top' );
	}
}
add_action( 'init', 'wormwood_posts_rewrite', 60 );

if ( ! function_exists( 'wormwood_change_archive_link' ) ) {
	/**
	 * Change Archive Permalink
	 *
	 * @param string  $link - the current link.
	 * @param integer $cat_id - the Post object.
	 *
	 * @return string|void
	 */
	function wormwood_change_archive_link( $link, $cat_id ) {

		$term       = get_term( $cat_id, 'category' );
		$slug       = $term->slug;
		$posts_name = wormwood_get_blog_page_name();

		$link = site_url( sprintf( '/%s/category/%s', $posts_name, $slug ) );

		return $link;
	}
}
add_filter( 'category_link', 'wormwood_change_archive_link', 100, 2 );

if ( ! function_exists( 'wormwood_archive_rewrite' ) ) {
	/**
	 * Add Blog Category rewrite
	 */
	function wormwood_archive_rewrite() {
		$posts_name = wormwood_get_blog_page_name();
		add_rewrite_rule( '^' . $posts_name . '/category/([^/]+)/?', 'index.php?category_name=$matches[1]', 'top' );
	}
}
add_action( 'init', 'wormwood_archive_rewrite', 50 );

if ( ! function_exists( 'wormwood_add_ajax_endpoints' ) ) {
	/**
	 * Register a rewrite endpoint for the API.
	 */
	function wormwood_add_ajax_endpoints() {
		add_rewrite_tag( '%ajax_action_name%', '([0-9]+)' );
		add_rewrite_rule( 'ajax/([^/]+)/?', 'index.php?ajax_action_name=$matches[1]', 'top' );
	}
}
add_action( 'init', 'wormwood_add_ajax_endpoints' );

if ( ! function_exists( 'wormwood_do_ajax' ) ) {
	/**
	 * Handle data (maybe) passed to the API endpoint.
	 */
	function wormwood_do_ajax() {
		global $wp_query;

		$action_name = $wp_query->get( 'ajax_action_name' );

		if ( ! empty( $action_name ) ) {
			check_ajax_referer( 'wormwood-ajax-nonce', 'ajax-nonce' );

			$action = 'wormwood_ajax_' . $action_name;
			$response = $action();

			echo $response; //@codingStandardsIgnoreLine
			exit;
		}
	}
}
add_action( 'template_redirect', 'wormwood_do_ajax' );

if ( ! function_exists( 'wormwood_get_blog_page_name' ) ) {
	/**
	 * Get the page slug for the Posts list page.
	 * @return string
	 */
	function wormwood_get_blog_page_name() {
		$posts_id   = get_option( 'page_for_posts' );

		if ( empty( $posts_id ) ) {
			return 'blog';
		}

		$posts_page = get_post( $posts_id );
		$posts_name = $posts_page->post_name;

		return $posts_name;
	}
}