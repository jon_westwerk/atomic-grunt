<?php
/**
 * Custom template tags
 *
 * @package wormwood
 */

if ( ! function_exists( 'wormwood_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function wormwood_posted_on() {

		$time_string = '<time class="entry__date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry__date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			esc_html_x( 'Posted on %s', 'post date', 'wormwood' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		$byline = sprintf(
			esc_html_x( 'by %s', 'post author', 'wormwood' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>';

	}
}

if ( ! function_exists( 'wormwood_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function wormwood_entry_footer() {

		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			$categories_list = get_the_category_list( esc_html__( ', ', 'wormwood' ) );
			if ( $categories_list && wormwood_categorized_blog() ) {
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'wormwood' ) . '</span>', $categories_list );
			}

			$tags_list = get_the_tag_list( '', esc_html__( ', ', 'wormwood' ) );
			if ( $tags_list ) {
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'wormwood' ) . '</span>', $tags_list );
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link( esc_html__( 'Leave a comment', 'wormwood' ), esc_html__( '1 Comment', 'wormwood' ), esc_html__( '% Comments', 'wormwood' ) );
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				esc_html__( 'Edit %s', 'wormwood' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);

	}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function wormwood_categorized_blog() {
	// Create an array of all the categories that are attached to posts.
	$all_the_cool_cats = get_categories( array(
		'fields'     => 'ids',
		'hide_empty' => 1,
		// We only need to know if there is more than one category.
		'number'     => 2,
	) );

	// Count the number of categories that are attached to the posts.
	$all_the_cool_cats = count( $all_the_cool_cats );

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so return true.
		return true;
	} else {
		// This blog has only 1 category so return false.
		return false;
	}
}

