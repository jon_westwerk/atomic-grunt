<?php
/**
 * Single posts template
 *
 * @package wormwood
 */

get_header(); ?>

	<div id="primary" class="primary">
		<div id="content" class="primary__content">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'parts/content', 'single' ); ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
